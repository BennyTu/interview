# Interview

## Descriptions

You need to impelment a simple Content Management System(CMS).
There are some functions it must have:
- Login
- Role
- Logout
- Different view with different roles

There is no UI SPECs here, you could do it as you like.

There are the UX SPECs you should follow as below.

- The Login page, user could login with account_name and password.

![](https://i.imgur.com/Vf7ptuT.png)

- If the user is a normal user, the user could see a simple page with menu. There are 2 options in menu for the normal user. The Home page will display the message "Hi <account_name>". The Logout option will be the logout function, if the user click it, it will do logout and redirect page to Login page. 

![](https://i.imgur.com/bggog7u.png)

- If the user is a admin user, the user could see a simple page with menu. There are 3 options in menu for the admin user. The Home page will display the message "Hi <account_name>". The Admin page will display the message "This is an admin page.". The Logout option will be the logout function, if the user click it, it will do logout and redirect page to Login page.

![](https://i.imgur.com/6X3GaCq.png)
![](https://i.imgur.com/jDwoxyN.png)




## Getting Started

### Install project

```shell
npm install
```

### Commands
**run at local**
```shell
npm run start
```

**test**
```shell
npm run test
```

**build**
```shell
npm run build
```

## Backend Documents

### Initialize backend in docker at background
```
docker-compose up -d
```

The backend server will be started at `8080` port after docker-compose up. 

Note. Please wait for 60s for starting the backend server when docker-compose up.


### Backend Restful API Dockments
**POST /v1/login**
Request Body:
```json
{
    "account": "<your_account_name>",
    "password": "<your_password"
}
```
200 OK Response Body:
```json
{
    "status": "ok",
    "data":{
        "token": "<authorized_token>",
        "role": "<admin|user>"
    }
}
```
400 unauth Response Body:
```json
{
    "status": "login failed",
    "data": null
}
```

**GET /v1/user**
Request Header:
```
Authorization: <your_login_token>"
```
200 ok Response Body:
```json
{
    "status": "ok",
    "data":{
        "account": "<account_name_with_token>",
        "role": "<admin|user>"
    }
}
```
400 unauth Response Body:
```json
{
    "status": "unauth",
    "data": null
}
```

**GET /v1/logout**
Request Header:
```
Authorization: <your_login_token>"
```
Response Body:
```json
{
    "status": "ok",
    "data": null
}
```

#### Create test login account
**POST /v1/create_account**
Request Body:
```json
{
    "account": "<your_account_name>",
    "password": "<your_password",
    "role": "<admin|user>"
}
```
200 OK Response Body:
```json
{
    "status": "ok",
    "data":{
        "account": "<account_name>",
        "role": "<admin|user>"
    }
}
```

