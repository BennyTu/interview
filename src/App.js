import './App.css';
import React, { useState } from 'react';
import {
  BrowserRouter,
  Route,
  Link,
  Switch
} from "react-router-dom";

import guestRoutes from './routes/guestRoutes';
import userRoutes from './routes/userRoutes';
import adminRoutes from './routes/adminRoutes';
import AuthRoute from './components/AuthRoute';

function App() {

    const role = localStorage.getItem('role') ?? "guest";
const [user, setUser] = useState({role: [role]});

  const loginAsUser = () => {
    setUser({
      role: ['user']
    });
  }

  const loginAsAdmin = () => {
    setUser({
      role: ['user', 'admin']
    });
  }

  return (
    <BrowserRouter>
      <Switch>
              {
                guestRoutes.map(
                (route) => <AuthRoute key={route.path} {...route} user={user}/>
              )}
              {
                (role == "user" || role == "guest") &&
                userRoutes.map(
                (route) => <AuthRoute key={route.path} {...route} user={user}/>
              )}
              {
                (role == "admin" || role == "guest") &&
                adminRoutes.map(
                (route) => <AuthRoute key={route.path} {...route} user={user}/>
              )}
      </Switch>
    </BrowserRouter>
  );
}

export default App;