import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import MySider from "../components/MySider";

const { Header, Footer, Sider, Content } = Layout;

function Admin() {

  return (
    <Layout>
      <Header>Header</Header>
      <Layout>
        <Sider>
            <MySider />
        </Sider>
        <Content>This is and admin page</Content>
      </Layout>
      <Footer>Footer</Footer>
    </Layout>
  );
}

export default Admin;