import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import MySider from "../components/MySider";

const { Header, Footer, Sider, Content } = Layout;

function Home() {
    const role = localStorage.getItem('role');
    const name = localStorage.getItem('name');

  return (
    <Layout>
      <Header>Header</Header>
      <Layout>
        <Sider>
            <MySider />
        </Sider>
        <Content>Hi {role} {name}</Content>
      </Layout>
      <Footer>Footer</Footer>
    </Layout>
  );
}

export default Home;