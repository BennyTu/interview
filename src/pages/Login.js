import React, { useState} from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button } from 'antd';
import { login, getUser } from "../actions/auth";
import { useQuery, useMutation } from 'react-query'


function Login(props) {
    const {loginAsUser, loginAsAdmin} = props;
    const [error, setError] = useState(null);


                                                //

    const mutation = useMutation(login, {
                                            onSuccess: (data) => {
                                                console.log(data);
                                                localStorage.setItem('token', data.data.token);
                                                getUserResult.mutate(data.data.token)
                                            },
                                            onError: (error) => {
                                                setError(error.response.data.status)
                                            },
                                        });

    const getUserResult = useMutation(getUser, {
                                                onSuccess: (data) => {
                                                    console.log(data);
                                                    localStorage.setItem('name', data.data.account);
                                                    localStorage.setItem('role', data.data.role);
                                                    window.location.replace('/')
                                                },
                                                onError: (error) => {
                                                    setError(error.response.data.status)
                                                },
                                            });

    const onFinish = values => {
        mutation.mutate(values)
    };

  return (
    <div className="login-bg">
        <div className="form">
            <div className="login-card">
                <div style={{textAlign: "center", paddingBottom: "20px", fontSize: "24px"}}>Login</div>
                <Form
                      name="basic"
                      labelCol={{ span: 8 }}
                      wrapperCol={{ span: 16 }}
                      initialValues={{ remember: true }}
                      onFinish={onFinish}
                    >
                      <Form.Item
                        label="User Name"
                        name="account"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                      >
                        <Input.Password />
                      </Form.Item>
                      {
                        error &&
                        <div style={{color: "red", textAlign: "center", paddingBottom: "20px"}}>Error Message: {error}</div>
                      }
                      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                          Login
                        </Button>
                      </Form.Item>
                </Form>
                <Link to="/signUp">Sign Up</Link>
            </div>
        </div>
    </div>
  );
}

export default Login;