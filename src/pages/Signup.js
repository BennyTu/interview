import React, { useState} from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Radio } from 'antd';
import { signUp } from "../actions/auth";
import { useMutation } from 'react-query'


function SignUp() {
    const [error, setError] = useState(null);

    const mutation = useMutation(signUp, {
                                            onSuccess: data => {
                                                console.log(data);
                                                alert("Sign Up Success")
                                                window.location.replace('/login')
                                            },
                                            onError: (error) => {
                                                setError(error.response.data.status)
                                            },
                                        });

    const onFinish = values => {
    console.log(values)
        mutation.mutate(values)
    };

  return (
    <div className="login-bg">
        <div className="form">
            <div className="login-card">
                <div style={{textAlign: "center", paddingBottom: "20px", fontSize: "24px"}}>Sign Up</div>
                <Form
                      name="basic"
                      labelCol={{ span: 8 }}
                      wrapperCol={{ span: 16 }}
                      initialValues={{ role: "user" }}
                      onFinish={onFinish}
                    >

                      <Form.Item
                        label="Role"
                        name="role"
                        initialvalues={"user"}
                      >
                        <Radio.Group >
                            <Radio value={"user"}>User</Radio>
                            <Radio value={"admin"}>Admin</Radio>>
                        </Radio.Group>

                      </Form.Item>

                      <Form.Item
                        label="User Name"
                        name="account"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                      >
                        <Input.Password />
                      </Form.Item>
                      {
                        error &&
                        <div style={{color: "red", textAlign: "center", paddingBottom: "20px"}}>Error Message: {error}</div>
                      }
                      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                          SignUp
                        </Button>
                      </Form.Item>
                </Form>
                <Link to="/login">Login</Link>
            </div>
        </div>
    </div>

  );
}

export default SignUp;