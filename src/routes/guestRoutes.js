import Login from '../pages/Login';
import SignUp from '../pages/SignUp';
import Home from '../pages/Home';

const guestRoutes = [
    {
        path: '/login',
        component: Login,
        exact: true,
        role: 'guest',
        backUrl: '/home'
    },
    {
        path: '/signUp',
        component: SignUp,
        exact: true,
        role: 'guest',
        backUrl: '/home'
    }
];

export default guestRoutes;