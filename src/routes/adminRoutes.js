import Home from '../pages/Home';
import Admin from '../pages/Admin';

const adminRoutes = [
  {
    path: '/home',
    component: Home,
    exact: true,
    role: 'admin',
    backUrl: '/login'
  },
  {
    path: '/admin',
    component: Admin,
    exact: true,
    role: 'admin',
    backUrl: '/login'
  },
  {
    path: '/',
    component: Home,
    exact: true,
    role: 'admin',
    backUrl: '/login'
  },
];

export default adminRoutes;