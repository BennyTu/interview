import Home from '../pages/Home';

const userRoutes = [
  {
    path: '/home',
    component: Home,
    exact: true,
    role: 'user',
    backUrl: '/login'
  },
  {
    path: '/',
    component: Home,
    exact: true,
    role: 'user',
    backUrl: '/login'
  },
];

export default userRoutes;