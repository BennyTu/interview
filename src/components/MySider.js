import React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu, Icon } from 'antd';

const { SubMenu } = Menu;
import { logout } from "../actions/auth";
import { useMutation } from 'react-query'



function MySider() {

    const role = localStorage.getItem('role');


    const logoutResult = useMutation(logout, {
                                                onSuccess: (data) => {

                                                    console.log(data);
                                                    localStorage.clear();
                                                    window.location.replace('/login')
                                                },
                                                onError: (error) => {
                                                    setError(error.response.data.status)
                                                },
                                            });


    const doLogout = () => {
        const t = localStorage.getItem('token');
        logoutResult.mutate(t);
    }

    return (
        <Menu>
            <Menu.Item key="1"><NavLink to="/home">Home</NavLink></Menu.Item>
            {
                role == "admin" &&
                <Menu.Item key="2"><NavLink to="/admin">Admin</NavLink></Menu.Item>
            }
            <Menu.Item key="3" onClick={() => { doLogout() }}>Log Out</Menu.Item>
        </Menu>
    );

}

export default MySider;

