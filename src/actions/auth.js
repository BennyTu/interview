import API from '../db/api';


export const login = async (params) => {
    try {
        const url = "/v1/login"
        const response = await API.post(url, params);

        return response.data;
    }
    catch (e) {
        throw new Error(e.response.statusText);
    }
};


export const signUp = async (params) => {
    try {
        const url = "/v1/create_account"
        const response = await API.post(url, params);

        return response.data;
    }
    catch (e) {
        throw new Error(e.response.statusText);
    }
};



export const getUser = async (token) => {
    try {
        const url = "/v1/user"
        const response = await API.get(url, {
            headers: { 'Authorization': token }
        });

        return response.data;
    }
    catch (e) {
        console.log(e.response)
        throw new Error(e.response.statusText);
    }
};


export const logout = async (token) => {
    try {
        const url = "/v1/logout"
        const response = await API.get(url, {
            headers: { 'Authorization': token }
        });

        return response.data;
    }
    catch (e) {
        console.log(e.response)
        throw new Error(e.response.statusText);
    }
};

